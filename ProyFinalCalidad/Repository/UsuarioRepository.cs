using System.Linq;
using System.Security.Claims;
using ProyFinalCalidad.Models;

namespace ProyFinalCalidad.Repository
{
    public interface IUsuarioRepository
    {
        public Usuario EncontrarUsuario(string username, string password);
        public Usuario UsuarioLogeado(Claim claim);
    }

    public class UsuarioRepository : IUsuarioRepository
    {
        private ProyFinalContext context;

        public UsuarioRepository(ProyFinalContext context)
        {
            this.context = context;
        }

        public Usuario EncontrarUsuario(string username, string password)
        {
            var usuario = context._Usuarios
                .FirstOrDefault(o => o.Username == username && o.Password == password);

            return usuario;
        }

        public Usuario UsuarioLogeado(Claim claim)
        {
            var user = context._Usuarios.FirstOrDefault(o => o.Username == claim.Value);
            return user;
        }
    }
}
