using System;
using System.Collections.Generic;

namespace ProyFinalCalidad.Models
{
    public class Curso
    {
        public int Id { get; set; }
        public int IdCategoria { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaCreacion{ get; set; }

        public List<DetalleUsuarioCurso> DetalleUsuarioCursos { get; set; }

        public List<Video> Videos { get; set;  }

        public Categoria categoria { get; set; }

        public List<Curso> Cursos { get; set; }
    }
}