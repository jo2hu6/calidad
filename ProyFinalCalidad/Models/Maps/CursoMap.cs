using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ProyFinalCalidad.Models.Maps
{
    public class CursoMap: IEntityTypeConfiguration<Curso>
    {
        public void Configure(EntityTypeBuilder<Curso> builder)
        {
            builder.ToTable("Curso");
            builder.HasKey(o => o.Id);

            builder.HasMany(o => o.Videos).WithOne(o => o.curso).HasForeignKey(o => o.IdCurso);
            
            builder.HasMany(o => o.DetalleUsuarioCursos).WithOne(o => o.curso).HasForeignKey(o => o.IdCurso);

            
        }
    }
}
