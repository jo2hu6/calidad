using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ProyFinalCalidad.Models.Maps
{
    public class CategoriaMap: IEntityTypeConfiguration<Categoria>
    {
        public void Configure(EntityTypeBuilder<Categoria> builder)
        {
            builder.ToTable("Categoria");
            builder.HasKey(o => o.Id);

            builder.HasMany(o => o.Cursos).WithOne(o => o.categoria).HasForeignKey(o => o.IdCategoria);
        }
    }
}
