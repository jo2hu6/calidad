using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ProyFinalCalidad.Models.Maps
{
    public class AmistadMap: IEntityTypeConfiguration<Amistad>
    {
        public void Configure(EntityTypeBuilder<Amistad> builder)
        {
            builder.ToTable("Amistad");
            builder.HasKey(o => o.Id);

            builder.HasMany(o => o.Mensajes).WithOne(o => o.amistad).HasForeignKey(o => o.IdAmistad);
        }
    }
}
