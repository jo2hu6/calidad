using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ProyFinalCalidad.Models.Maps
{
    public class UsuarioMap: IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.ToTable("Usuario");
            builder.HasKey(o => o.Id);

            
            //amistad
            builder.HasMany(o => o.Amistad1).WithOne(o => o.UsuarioAmigo).HasForeignKey(o => o.IdUsuario1);
            builder.HasMany(o => o.Amistad2).WithOne(o => o.UsuarioLocal).HasForeignKey(o => o.IdUsuario2);

            //DetalleUsuarioCurso
            builder.HasMany(o => o.DetalleUsuarioCursos).WithOne(o => o.usuario).HasForeignKey(o => o.IdUsuario);

        }
    }
}
