using System.Collections.Generic;

namespace ProyFinalCalidad.Models
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Nombres { get; set; }

        public List<Amistad> Amistad1 { get; set; }
        public List<Amistad> Amistad2 { get; set; }

        public List<DetalleUsuarioCurso> DetalleUsuarioCursos { get; set; }

    }
}