using System;
using System.Collections.Generic;

namespace ProyFinalCalidad.Models
{
    public class Amistad
    {
        public int Id { get; set; }
        public int IdUsuario1 { get; set; }
        public int IdUsuario2 { get; set; }
        public DateTime FechaCreacion { get; set; }

        public List<Mensaje> Mensajes { get; set; }

        public Usuario UsuarioLocal { get; set; }
        public Usuario UsuarioAmigo { get; set; }
        

    }
}