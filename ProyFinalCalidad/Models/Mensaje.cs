using System;

namespace ProyFinalCalidad.Models
{
    public class Mensaje
    {
        public int Id { get; set; }
        public int IdAmistad { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaCreacion{ get; set; }
        public int IdUsuario1Creador { get; set; }
        public int IdUsuario2Receptor { get; set; }
        public Amistad amistad { get; set; }

    }
}